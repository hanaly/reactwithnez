import { useState } from "react";

function App() {
  const [age,setAge]=useState(12)
  console.log('age abony',age);
  return (
    <div className="container">
      <h1>
      {age}
      </h1>
      <button onClick={() =>{
        setAge(age+1)
        //console.log('age au moment du click',age)
      }}>age +1</button>
    </div>
  )
}

export default App
