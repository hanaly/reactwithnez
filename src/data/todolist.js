const todoList = [
    {
      id: 1,
      task: "Faire les courses",
      completed: false
    },
    {
      id: 2,
      task: "Faire le ménage",
      completed: false
    },
    {
      id: 3,
      task: "Terminer le projet React",
      completed: true
    },
    {
      id: 4,
      task: "Appeler le client",
      completed: false
    },
    {
      id: 5,
      task: "Aller à la salle de sport",
      completed: true
    }
  ];
  
  export default todoList;
  